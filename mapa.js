let miMapa = L.map('mapid');

miMapa.setView([4.6500027,-74.170230], 16);

let miProveedor= L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
});
miProveedor.addTo(miMapa);

let miMarcador = L.marker([4.652329, -74.170547]);
miMarcador.addTo(miMapa)



var popup = L.popup();

miMarcador.bindPopup(" FRIJOL</b><br> Vigna Umbellata").openPopup();


//json:conjunto de atributos definidos dentro decorchetes (objetos)

//GeoJSON:describe objetos geograficos 
// se puedes describir puntos(point)
//lineas 
//poligono
//multiples lineas
//multiples poligonos 
//geometry collection

//objeto geografico +metadata(otros atributos no geograficos)= entidad (Features)


L.geoJSON(sitio).addTo(miMapa)


let miTitulo=document.getElementById("titulo");
miTitulo.textContent=sitio.features[0].properties.popupContent;

let miTitulO=document.getElementById("description");
miTitulO.textContent=sitio.features[0].properties.popupContent;